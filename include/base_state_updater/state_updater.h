#pragma once

#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float64.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/UInt32.h>
#include <geometry_msgs/Quaternion.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
#include <math.h>
#include <Eigen/Dense>
#include <tf/LinearMath/Quaternion.h>

using namespace std;
using Eigen::MatrixXd;

namespace base_state_updater{

    class State_Updater
    {
        private:
            ros::NodeHandle n;
            const double NODE_RATE = 50;

            std::string base_frame_id = "base_link";
            std::string odom_frame_id = "odom";

            ros::Publisher wheel_odom;
            ros::Publisher fl_wheel_feedback;
            ros::Publisher fr_wheel_feedback;
            ros::Publisher bl_wheel_feedback;
            ros::Publisher br_wheel_feedback;

            ros::Subscriber fl_encoder_subscriber; 
            ros::Subscriber fr_encoder_subscriber; 
            ros::Subscriber bl_encoder_subscriber; 
            ros::Subscriber br_encoder_subscriber; 

            const double TICKS_PER_REV = 627;
            const float WHEEL_X_DIST = 0.2032; 
            const float WHEEL_Y_DIST = 0.2032;

            const double TICKS_PER_RAD = (2*M_PI)/TICKS_PER_REV;
            const float WHEEL_RADIUS = 0.0508; //meters
            const float WHEEL_CIRCUM = 2 * WHEEL_RADIUS * M_PI;

            
            const double TICKS_PER_METER = (1/WHEEL_CIRCUM)*TICKS_PER_REV;
            const double BASE_WIDTH = 0.4572;
            const float C1 = WHEEL_X_DIST + WHEEL_Y_DIST;
            
            const float ENCODER_RESOLUTION = TICKS_PER_REV*GEAR_RATIO_MOTOR; 
            const float ENC_RES_PER_DEG = 360/TICKS_PER_REV; //3929 Ticks per 1 meter
            const double GEAR_RATIO_WHEEL = 0.75; //18T / 24T
            const float GEAR_RATIO_MOTOR = 1.25; //18T / 24T
            const float MOTOR_MAX_RPM = 100;
            const float WHEEL_MAX_RPM = MOTOR_MAX_RPM * GEAR_RATIO_WHEEL;

            std_msgs::Float64 fl_wheel, fr_wheel, bl_wheel, br_wheel;
            double wheel_angular_velocity;

            tf::TransformBroadcaster odom_broadcaster;
        //Encoder related variables
            double encoder_min;
            double encoder_max;

            double encoder_low_wrap;
            double encoder_high_wrap;

            int fl_encoder, prev_fl_encoder;
            int fr_encoder, prev_fr_encoder;
            int bl_encoder, prev_bl_encoder;
            int br_encoder, prev_br_encoder;

            int fr_left, fr_right, bck_left, bck_right;

            double fr_curr_speed, fl_curr_speed, bl_curr_speed, br_curr_speed;
            double fr_curr_dist, fl_curr_dist, bl_curr_dist, br_curr_dist;

            float curr_Vx,curr_Vy, curr_Wo;
            float curr_x, curr_y, curr_wo;

            ros::Duration t_delta;
            ros::Time t_next, then, dt, current_time, last_time;

            double x_final,y_final, theta_final;

        public:
            State_Updater()
            {
                ROS_INFO("Started odometry computing node");
                init_variables();
                wheel_odom = n.advertise<nav_msgs::Odometry>("/base/odom", 1);

                fl_wheel_feedback = n.advertise<std_msgs::Float64>("/base/fl_wheel/feedback", 1);
                fr_wheel_feedback = n.advertise<std_msgs::Float64>("/base/fr_wheel/feedback", 1);
                bl_wheel_feedback = n.advertise<std_msgs::Float64>("/base/bl_wheel/feedback", 1);
                br_wheel_feedback = n.advertise<std_msgs::Float64>("/base/br_wheel/feedback", 1);

                fl_encoder_subscriber = n.subscribe("/base/fl_wheel/encoder", 10, &State_Updater::fl_encoder_callback, this);
                fr_encoder_subscriber = n.subscribe("/base/fr_wheel/encoder", 10, &State_Updater::fr_encoder_callback, this);
                bl_encoder_subscriber = n.subscribe("/base/bl_wheel/encoder", 10, &State_Updater::bl_encoder_callback, this);
                br_encoder_subscriber = n.subscribe("/base/br_wheel/encoder", 10, &State_Updater::br_encoder_callback, this);   
            }
        //Functions();
            void init_variables();
            void spin();
            void fl_encoder_callback(const std_msgs::UInt32::ConstPtr& msg);
            void fr_encoder_callback(const std_msgs::UInt32::ConstPtr& msg);
            void bl_encoder_callback(const std_msgs::UInt32::ConstPtr& msg);
            void br_encoder_callback(const std_msgs::UInt32::ConstPtr& msg);

            int    wheel_direction(int enc_value);
            double enc_to_deg(int enc_value);
            double rad_to_rev(double rad);
            double update_wheel_displacement(int *enc_value, int wheel);
            double update_wheel_velocity(ros::Time time, ros::Time next_time, ros::Time then_time, double enc_d, int *enc_value, int wheel);

            void  pub_feedback(ros::Time now);
            void  pub_odom();
            void  mecanum_fw_kinematics(float fl_W1, float fr_W2, float bl_W3, float br_W4, float *Vx, float *Vy, float *Wo);
            void  mecanum_inv_kinematics(float Vx, float Vy, float Wo, float *fl_W1, float *fr_W2, float *bl_W3, float *br_W4);
    };
};