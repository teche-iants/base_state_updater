#include<base_state_updater/state_updater.h>

namespace base_state_updater{
    void State_Updater::init_variables(){
        //update_prev_time, prev_enc_value1, prev_enc_value2, prev_enc_value3, prev_enc_value4, wheel_enc_dt = 0;
        // intialize integrators
        ROS_INFO("Init Vars");

        fl_encoder, prev_fl_encoder = 0;
        fr_encoder, prev_fr_encoder = 0;
        bl_encoder, prev_bl_encoder = 0;
        br_encoder, prev_br_encoder = 0;

        fr_left   = 0;
        fr_right  = 0;
        bck_left  = 0;
        bck_right = 0;

        encoder_min =  0;
        encoder_max =  4294967295;

        encoder_low_wrap = ((encoder_max - encoder_min) * 0.3) + encoder_min ;
        encoder_high_wrap = ((encoder_max - encoder_min) * 0.7) + encoder_min ;

        t_delta = ros::Duration(1.0 / NODE_RATE);
        ROS_INFO_STREAM("t_delta = " << t_delta);
        t_next = ros::Time::now() + t_delta;
        ROS_INFO_STREAM("t_next = " << t_next);

        then = ros::Time::now();

        x_final, y_final, theta_final = 0;
        fr_curr_speed, fl_curr_speed, bl_curr_speed, br_curr_speed = 0;
        fr_curr_dist, fl_curr_dist, bl_curr_dist, br_curr_dist = 0;
        curr_Vx,curr_Vy, curr_Wo = 0;
        curr_x, curr_y, curr_wo  = 0;
    }

    void State_Updater::spin(){
        ros::Rate loop_rate(NODE_RATE);
        ROS_INFO("******Entering Loop************");
        while(ros::ok()){ 
            pub_odom();
            ROS_INFO("-------------");
            ros::spinOnce();
            loop_rate.sleep();
        }
    }

    void State_Updater::fl_encoder_callback(const std_msgs::UInt32::ConstPtr& msg){
        fl_encoder = msg->data;
    }

    void State_Updater::fr_encoder_callback(const std_msgs::UInt32::ConstPtr& msg){
        
        int enc = msg->data;
        fr_right = enc;
    }

    void State_Updater::bl_encoder_callback(const std_msgs::UInt32::ConstPtr& msg){
        bl_encoder = msg->data;
    }

    void State_Updater::br_encoder_callback(const std_msgs::UInt32::ConstPtr& msg){
        br_encoder = msg->data;
    }

    int State_Updater::wheel_direction(int enc_value){
        /*ROS_INFO("ls = %i", ls);
        if(enc_value> prev_enc_value3 & enc_value <= 103){
            ROS_INFO("+VE");
            wheel_direc = 1; //CCW
            prev_enc_value3 = enc_value;
            return enc_value;
        }
        else if(enc_value < prev_enc_value3 ){
            ROS_INFO("-VE");
            wheel_direc = -1; //CW
        }
        else if(enc_value == prev_enc_value3){
            ROS_INFO("0");
            wheel_direc = 0; //Not spinning
            prev_enc_value3 = enc_value;
            return enc_value;
        }
        else if (enc_value == prev_enc_value3 & ls > 103){
            
        }
        else if(enc_value> prev_enc_value3 & enc_value > 103 || enc_value> prev_enc_value3 & ls > 103){
            //enc_value = 104;
            ROS_INFO("Exceeded");
            prev_enc_value3 = ls;
            return ls = (2*104) + enc_value; 
        }
        prev_enc_value3 = enc_value;*/

        
    }

    double State_Updater::enc_to_deg(int enc_value){ 
        double enc_deg_to_rad = enc_value * ENC_RES_PER_DEG;
        //ROS_INFO("enc_deg_to_rad = %f", enc_deg_to_rad);
        return enc_deg_to_rad;
    }

    double State_Updater::rad_to_rev(double rad){
        return rad / (2*M_PI);
    }

    double State_Updater::update_wheel_displacement(int *enc_value, int wheel){
        double d_enc = (wheel - *enc_value) / (TICKS_PER_METER);
        return d_enc;
    }

    double State_Updater::update_wheel_velocity(ros::Time time, ros::Time next_time, ros::Time then_time, double enc_d, int *enc_value, int wheel){
        ros::Time now = time;
        double elapsed;
        double d_enc, d;
        int enc = *enc_value;

        if (now > next_time) {
            elapsed = now.toSec() - next_time.toSec();
            then_time = now; 
        /*
            ROS_INFO_STREAM("now = " << now);
            ROS_INFO_STREAM("t_next = " << t_next);*/
            ROS_INFO_STREAM("elapsed = " << t_delta.toSec());

            if(*enc_value == 0){
			d_enc = 0;
            //fr_encoder = wheel;
            ROS_INFO_STREAM("enc_value = 0");
		    }
            else{
                d_enc = enc_d;
                ROS_INFO_STREAM("TICKS_PER_METER = " << TICKS_PER_METER);
                ROS_INFO_STREAM("wheel = " << wheel);
                ROS_INFO_STREAM("enc_value = " << *enc_value);
                ROS_INFO_STREAM("d_enc = " << d_enc);
		    }
            *enc_value = wheel;

            d = d_enc/t_delta.toSec();
            d = d / WHEEL_RADIUS; // m/s
            ROS_INFO_STREAM("d= " << d);
            ROS_INFO("------------");
            return d/1.33; //m/s 
        }
    }

    void State_Updater::pub_feedback(ros::Time now){
        //THis is used to pub wheel angular velocity for PID
        fl_curr_dist = update_wheel_displacement(&fl_encoder, fr_left);
        fr_curr_dist = update_wheel_displacement(&fr_encoder, fr_right);
        bl_curr_dist = update_wheel_displacement(&bl_encoder, bck_left);
        br_curr_dist = update_wheel_displacement(&br_encoder, bck_right);

        fl_curr_speed = update_wheel_velocity(now, t_next, then, fl_curr_dist, &fl_encoder, fr_left);
        fr_curr_speed = update_wheel_velocity(now, t_next, then, fr_curr_dist, &fr_encoder, fr_right);
        bl_curr_speed = update_wheel_velocity(now, t_next, then, bl_curr_dist, &bl_encoder, bck_left);
        br_curr_speed = update_wheel_velocity(now, t_next, then, br_curr_dist, &br_encoder, bck_right);
        
        fl_wheel.data = fl_curr_speed;
        fr_wheel.data = fr_curr_speed;
        bl_wheel.data = bl_curr_speed;
        br_wheel.data = br_curr_speed;

        fl_wheel_feedback.publish(fl_wheel);
        fr_wheel_feedback.publish(fr_wheel);
        bl_wheel_feedback.publish(bl_wheel);
        br_wheel_feedback.publish(br_wheel);
        
    }

    void State_Updater::pub_odom(){
        //Used for Odometery 
        ros::Time curr_time = ros::Time::now(); 
        pub_feedback(curr_time); //This will calc the indvidiual speed of each wheel

		mecanum_fw_kinematics((float)fl_curr_speed, (float)fr_curr_speed, (float)bl_curr_speed, (float)br_curr_speed, &curr_Vx, &curr_Vy, &curr_Wo); // This will give us the current overall velocity of the robot from the wheel encoders
        
        if(curr_Vx != 0 || curr_Vy != 0 || curr_Wo != 0){ //If the robot is moving 
            mecanum_fw_kinematics(fl_curr_dist,fr_curr_dist,bl_curr_dist,br_curr_dist,&curr_x, &curr_y, &curr_wo);  //Calculate the robots current position
            x_final += curr_x * cos(curr_wo) - curr_y * sin(curr_wo);
            y_final += curr_x * sin(curr_wo) + curr_y * cos(curr_wo);
            theta_final += curr_wo;
            theta_final = fmod(theta_final, 2*M_PI);
        }
        
        geometry_msgs::Quaternion quaternion = tf::createQuaternionMsgFromYaw(theta_final);

        //first, we'll publish the transform over tf
        geometry_msgs::TransformStamped odom_trans;
        odom_trans.header.stamp = curr_time;
        odom_trans.header.frame_id = odom_frame_id;
        odom_trans.child_frame_id  = base_frame_id;

        odom_trans.transform.translation.x = x_final; 
        odom_trans.transform.translation.y = y_final; 
        odom_trans.transform.translation.z = 0.0;
        odom_trans.transform.rotation = quaternion;

        //send the transform
        odom_broadcaster.sendTransform(odom_trans);

        //next, we'll publish the odometry message over ROS
        nav_msgs::Odometry odom;
        odom.header.stamp = curr_time;
        odom.header.frame_id = odom_frame_id;

        //set the position
        odom.pose.pose.position.x = y_final; //x_final
        odom.pose.pose.position.y = y_final; //y_final
        odom.pose.pose.position.z = 0.0;
        odom.pose.pose.orientation = quaternion;

        //set the velocity
        odom.child_frame_id = base_frame_id;
        odom.twist.twist.linear.x  = curr_Vx; //dx
        odom.twist.twist.linear.y  = curr_Vx; //dy
        odom.twist.twist.linear.z  = 0.0; //the robot does not fly
        odom.twist.twist.angular.z = curr_Wo; //dr

        //publish the message
        wheel_odom.publish(odom);
    }

    // To be used to estimate robot's speed
    void State_Updater::mecanum_fw_kinematics(float fl_W1, float fr_W2, float bl_W3, float br_W4, float *Vx, float *Vy, float *Wo){
        MatrixXd Vw(4,1);
        Vw << fl_W1,
              fr_W2,
              bl_W3,
              br_W4;

        MatrixXd Vo(3,1); //generalized velocity of the center point of the robot

        MatrixXd J(3,4);
        J <<  1,    1,     1,    1,
             -1,    1,     1,   -1,
             -1/C1, 1/C1, -1/C1, 1/C1;

        Vo = (WHEEL_RADIUS/4) * J * Vw;

        *Vx = Vo(0,0);
        *Vy = Vo(1,0);
        *Wo = Vo(2,0);
    }

    void State_Updater::mecanum_inv_kinematics(float Vx, float Vy, float Wo, float *fl_W1, float *fr_W2, float *bl_W3, float *br_W4){
        MatrixXd Wv(4,1);
        MatrixXd Vo(3,1); //generalized velocity of the center point of the robot
        Vo << Vx,
              Vy,
              Wo;

        MatrixXd J(4,3);
        J << 1,  1,  C1,
             1, -1, -C1,
             1,  1, -C1,
             1, -1,  C1;


        Wv = (1/WHEEL_RADIUS) * J * Vo;

        *fl_W1 = Wv(1,0);
        *fr_W2 = Wv(0,0);
        *bl_W3 = Wv(2,0);
        *br_W4 = Wv(3,0);
        //Wheel Vel in m/s
    }
};