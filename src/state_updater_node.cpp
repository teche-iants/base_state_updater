#include<base_state_updater/state_updater.h>

using namespace base_state_updater;

int main(int argc, char **argv)
{
	ros::init(argc, argv, "state_updater");
	ros::NodeHandle n;
	State_Updater m_;
	m_.spin();
	return 0;
}